TITLE Final Program
PAGE 80,160
;
;	Author:	Brennan Toles
;	Simple calculator that performs addition, subtraction, multiplication and division of numbers 255 and below.
;
CR	EQU	0DH
LF	EQU	0AH
EOT	EQU '$'
SPACE EQU ' '
PLUS EQU '+'
MINUS EQU '-'
MULT EQU '*'
DIVI EQU '/'
;
	JMP START
;

PROMPT DB "Please enter your operation: ", EOT
CHECK1 DB "checkl", CR, LF, EOT
CHECK2 DB "check2", CR, LF, EOT
CHECK3 DB "check3", CR, LF, EOT
CHECK4 DB "check4", CR, LF, EOT
CRLF DB CR, LF, EOT
NEGPROMPT DB "-", EOT
PROMPTD DB " R ", EOT

INPUT DB 15, ?, 15 DUP ?
ANUM1 DB 4 DUP 30H
ANUM2 DB 6 DUP 30H
BNUM1 DB 0
BNUM2 DB 0
OP DB ?
ANSWERW DB 8 DUP 30H
ANSWERB DB 6 DUP 30H

NUMBYTES DB 0
OPERAND DB 0
	
START:
	MOV [ANUM2+3], CR
	MOV [ANUM2+4], LF
	MOV [ANUM2+5], '$'
	MOV [ANUM1+3], EOT
	MOV DI, 0
	
	LEA DX, PROMPT
	MOV AH, 09H
	INT 21H
	
	MOV AH, 0AH
	MOV DX, OFFSET INPUT
	INT 21H
	
	
	MOV INPUT[12], CR
	MOV INPUT[13], LF
	MOV INPUT[14], '$'
	
	;POINT TO INPUT
	;LEA DI, INPUT+2
	
OUTER:
	; SET NUMBER OF BYTES TO ZERO
	MOV CX, 0
	; INCREASE OPERAND COUNT
	INC OPERAND
	
INNER:
	;IF SPACE, FIRST NUMBER IS DONE
	CMP INPUT[DI+2], SPACE
	JE BREAK
	
	;INCREASE POINTER AND NUMBYTES
	INC DI
	INC CX
	JMP INNER
	
COMPUTE3:
	CMP OPERAND, 02
	JE SAVE3
	
	; LOAD CHARS INTO ASCII BUFFER
	LEA SI, ANUM1
	MOV BH, INPUT[DI-1]
	MOV [SI], BH
	MOV BH, INPUT[DI]
	MOV [SI+1], BH
	MOV BH, INPUT[DI+1]
	MOV [SI+2], BH
	
	CALL ASCBIN8

	; SAVE BINARY
	MOV [BNUM1], AL
	
	JMP READOP
	
SAVE3:
	LEA SI, ANUM2
	MOV BH, INPUT[DI-1]
	MOV [SI], BH
	MOV BH, INPUT[DI]
	MOV [SI+1], BH
	MOV BH, INPUT[DI+1]
	MOV [SI+2], BH
	
	CALL ASCBIN8
	; SAVE BINARY
	MOV [BNUM2], AL
	
	;CALL DUMPBIN8
	
	JMP CALCULATE
	
BREAK:
	CMP CX, 02
	JL COMPUTE1
	JE COMPUTE2
	JG COMPUTE3
	
COMPUTE2:
	CMP OPERAND, 02
	JE SAVE2
	
	LEA SI, ANUM1
	MOV BH, 30H
	MOV [SI], BH
	MOV BH, INPUT[DI]
	MOV [SI+1], BH
	MOV BH, INPUT[DI+1]
	MOV [SI+2], BH
	
	CALL ASCBIN8

	; SAVE BINARY
	MOV [BNUM1], AL
	
	;CALL DUMPBIN8
	
	JMP READOP
	
SAVE2:
	LEA SI, ANUM2
	MOV BH, 30H
	MOV [SI], BH
	MOV BH, INPUT[DI]
	MOV [SI+1], BH
	MOV BH, INPUT[DI+1]
	MOV [SI+2], BH
	
	CALL ASCBIN8
	; SAVE BINARY
	MOV [BNUM2], AL
	
	;CALL DUMPBIN8
	
	JMP CALCULATE
	
COMPUTE1:
	CMP OPERAND, 02
	JE SAVE1
	
	;COMPUTE FIRST OPERAND
	LEA SI, ANUM1
	MOV BL, INPUT[DI+1]
	MOV [SI], BL
	
	MOV BL, [ANUM1]
	SUB BL, 30H
	
	LEA SI, BNUM1
	MOV [SI], BL
	
	JMP READOP
	
SAVE1:
	;COMPUTE SECOND OPERAND
	LEA SI, ANUM2
	MOV BL, INPUT[DI+1]
	MOV [SI], BL
	
	MOV BL, [ANUM2]
	SUB BL, 30H
	
	LEA SI, BNUM2
	MOV [SI], BL
	
	JMP CALCULATE
		
READOP:
	;POINT TO OPERATOR
	INC DI
	
	;SAVE OPERATOR
	
	MOV SI, PLUS
	MOV BH, 0
	MOV BL, INPUT[DI+2]
	CMP BX, SI
	JE ADDITION
	
	
	MOV SI, MINUS
	MOV BH, 0
	MOV BL, INPUT[DI+2]
	CMP BX, SI
	JE SUBTRACT
	
	MOV SI, MULT
	MOV BH, 0
	MOV BL, INPUT[DI+2]
	CMP BX, SI
	JE MULTIPLY
	
	MOV SI, DIVI
	MOV BH, 0
	MOV BL, INPUT[DI+2]
	CMP BX, SI
	JE DIVIDE
	

ADDITION:
	;POINT TO BREAK OPERAND
	MOV OP, PLUS
	ADD DI, 2
	JMP OUTER
	
SUBTRACT:
	;POINT TO BREAK OPERAND
	MOV OP, MINUS
	ADD DI, 2
	JMP OUTER
	
MULTIPLY:
	;POINT TO BREAK OPERAND
	MOV OP, MULT
	ADD DI, 2
	JMP OUTER

DIVIDE:
	;POINT TO BREAK OPERAND
	MOV OP, DIVI
	ADD DI, 2
	JMP OUTER
	
CALCULATE:
	MOV AL, OP
	CMP AL, PLUS
	JE ADDEM
	
	CMP AL, MINUS
	JE SUBEM
	
	CMP AL, MULT
	JE MULEM
	
	CMP AL, DIVI
	JE DIVEM
	
	;CALCULATIONS
ADDEM:
	MOV AH, BNUM2
	MOV AL, BNUM1
	ADD AL, AH
	LEA SI, ANSWERW
	MOV AH, 0
	ADC AH, 0
	CALL BINASC16
	JMP OUTPUTW
SUBEM:
	MOV AH, 0
	MOV AL, BNUM1
	SUB AL, BNUM2
	JC NEGATIVE
	LEA SI, ANSWERB
	CALL BINASC8
	JMP OUTPUTB
MULEM:
	MOV AL, BNUM1
	MOV AH, BNUM2
	MUL AH
	LEA SI, ANSWERW
	CALL BINASC16
	JMP OUTPUTW
DIVEM:
	MOV AL, B[BNUM1]
	MOV AH, 0
	DIV B[BNUM2]
	LEA SI, ANUM1
	CALL BINASC8
	MOV AL, AH
	LEA SI, ANUM2
	CALL BINASC8
	JMP OUTPUTD
	
NEGATIVE:
	NEG AL
	LEA SI, ANSWERB
	CALL BINASC8
	JMP OUTPUTN
	
OUTPUTB:		
	;OUTPUT FOR SUBTRACTION
	LEA DX, CRLF
	MOV AH, 09H
	INT 21H
	LEA DX, INPUT+2
	INT 21H
	
	MOV [ANSWERB+3], CR
	MOV [ANSWERB+4], LF
	MOV [ANSWERB+5], EOT

	LEA DX, ANSWERB
	MOV AH, 09H
	INT 21H
	
	JMP EXIT
	
OUTPUTN:		
	;OUTPUT FOR SUBTRACTION (NEGATIVE RETURN)
	MOV AH, 09H
	LEA DX, CRLF
	INT 21H
	LEA DX, INPUT+2
	INT 21H
	LEA DX, NEGPROMPT
	INT 21H
	
	MOV [ANSWERB+3], CR
	MOV [ANSWERB+4], LF
	MOV [ANSWERB+5], EOT

	LEA DX, ANSWERB
	MOV AH, 09H
	INT 21H
	
	JMP EXIT
	
OUTPUTD:
	;DIVISION OUTPUT
	LEA DX, CRLF
	MOV AH, 09H
	INT 21H
	LEA DX, INPUT+2
	INT 21H
	LEA DX, ANUM1
	INT 21H
	LEA DX, PROMPTD
	INT 21H
	LEA DX, ANUM2
	INT 21H
	
	JMP EXIT
	
OUTPUTW:	
	;MULT AND ADDITION OUTPUT
	MOV [ANSWERW+5], CR
	MOV [ANSWERW+6], LF
	MOV [ANSWERW+7], EOT
	
	MOV AH, 09H
	LEA DX, CRLF
	INT 21H
	LEA DX, INPUT+2
	INT 21H
	LEA DX, ANSWERW
	INT 21H
	
EXIT:
	MOV AX, 4C00H
	INT 21H
	

;***********************************************************************************************
;       Subroutine ASCBIN8
;
;       A subroutine to convert 3 bytes of ASCII to 8 bit binary
;
;       Note: This version of the subroutine does not perform any
;       error testing. It assumes the input values are value ASCII
;       characters representing values between 0 and 255 decimal.
;       A separate subroutine could perform the validity tests.
;
;       ENTRY:  SI points to a 3 byte ASCII buffer
;               DI is used internally
;       OUTPUT1:   AL holds the 8 bit converted value
;               DI preserved
;       USE:    Input must be three bytes
;
ASCBIN8:
       	 PUSH	DI              	;save DI on entry
       	 LEA        	DI, MULT1  	;point to placeholder values
       	 SUB       	B[SI],30H      	;remove ASCII bias from numbers
       	 SUB       	B[SI+1],30H
       	 SUB       	B[SI+2],30H
       	 MOV      	CX,3            	;initialize loop counter
       	 MOV      	BL,0            	;initialize sum register
AB1: 
      	 MOV      	AL,[SI]         	;get first byte
       	 MOV      	AH,0           	;clear upper byte of AX
        	 MUL      	B[DI]          	;multiply by placeholder
       	 ADD      	BL,AL        	;save value
        	 INC        	DI              	;point to next place holder
        	 INC        	SI             	;point to next byte
      	 LOOP    	AB1
      	 MOV      	AL,BL          	 ;place sum into output register
       	 POP       	DI            	 	;restore DI
       	 RET
MULT1        DB          100,10,1
;
;********************************************************************************************S
;********************************************************************************************
;       Subroutine BINASC8
;
;       A subroutine to convert an 8 bit binary value to 3 bytes of
;       printable ASCII. No errors are detected in this module since none
;       are possible.
;
;       ENTRY:  AL holds the 8 bit binary value to be converted to ASCII
;               SI points to a 3 byte buffer to store the ASCII characters
;               DI is used internally as part of the conversion routine
;       OUTPUT1:   DI is preserved, AL destroyed
;
BINASC8:
	PUSH    	DI              	;save DI on entry
	MOV     	W[SI],3030H     	;preload ASCII buffer with number bias
	MOV     	B[SI+2],30H
	LEA     	DI, MULP  	;point to the place holder values
	MOV     	CX,2            	;set up count for loop iteration
B2A1:   	SUB     	AL,[DI]         	;subtract placeholder value
	JC      	B2A2            	;if negative, we subtracted too much
	INC     	B[SI]           	;if positive, add 1 to ASCII byte
	JMP     	B2A1            	;continue counting place values until minus
B2A2:  	ADD     	AL,B[DI]        	;restore AL to previous value oversubtracted
	INC     	DI              	;point to next place holder
	INC     	SI              	;point to next ASCII byte
	LOOP    	B2A1            	;continue for next place
	ADD     	B[SI],AL       	;add units to units byte bias
	POP     	DI              	;restore DI
	RET                     		;return to calling routine
MULP    	DB      	100,10
;
;*****************************************************************************************
;
;********************************************************************************************
;       Subroutine BINASC16
;
;       A subroutine to convert an 8 bit binary value to 3 bytes of
;       printable ASCII. No errors are detected in this module since none
;       are possible.
;
;       ENTRY:  AX holds the 8 bit binary value to be converted to ASCII
;               SI points to a 5 byte buffer to store the ASCII characters
;               DI is used internally as part of the conversion routine
;       OUTPUT1:   DI is preserved, AL destroyed
;
BINASC16:
	PUSH    	DI              	;save DI on entry
	MOV     	W[SI],3030H     	;preload ASCII buffer with number bias
	MOV     	B[SI+2],30H
	MOV     	B[SI+3],30H
	MOV     	B[SI+4],30H
	LEA     	DI, MULP6  	;point to the place holder values
	MOV     	CX,4            	;set up count for loop iteration
B2A16:   	
	SUB     	AX,[DI]         	;subtract placeholder value
	JC      	B2A26            	;if negative, we subtracted too much
	INC     	B[SI]           	;if positive, add 1 to ASCII byte
	JMP     	B2A16            	;continue counting place values until minus
B2A26:  	
	ADD     	AX,[DI]        	;restore AL to previous value oversubtracted
	INC     	DI              	;point to next place holder
	INC 		DI
	INC     	SI              	;point to next ASCII byte
	LOOP    	B2A16            	;continue for next place
	ADD     	B[SI],AL       	;add units to units byte bias
	POP     	DI              	;restore DI
	RET                     		;return to calling routine
MULP6    	DW      	10000,1000,100,10
;
;*****************************************************************************************
;
	END
